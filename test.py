#!/usr/bin/python3

import unittest
import xmlrunner
import subprocess
from subprocess import CalledProcessError, Popen, DEVNULL, PIPE, STDOUT


class DictionaryTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        subprocess.check_call(["make", "all"], stdout=DEVNULL, stderr=STDOUT)

    @classmethod
    def tearDownClass(cls):
        subprocess.check_call(["make", "clean"], stdout=DEVNULL, stderr=STDOUT)

    def _launch(self, exec_file, user_input):
        try:
            p = Popen(['./' + exec_file], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            (output, _) = p.communicate(user_input.encode())
            self.assertNotEqual(p.returncode, -11, 'segmentation fault')
            return output.decode(), p.returncode
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
            return exc.output.decode(), exc.returncode

    def test_key_exists(self):
        data = {
            "first key": "first explanation",
            "second key": "second explanation",
            "third key": "third explanation"
        }
        for key in data:
            output, ret = self._launch("main", key)
            self.assertEqual(ret, 0, f"Program returned {ret} while 0 expected")
            self.assertEqual(output, data[key], f"find_word('{key}'): expected '{data[key]}', got '{output}'")

    def test_key_absent(self):
        data = [" ", "absent", "another key"]
        err_string = "No such key string"
        for key in data:
            output, ret = self._launch("main", key)
            self.assertEqual(ret, 1, f"Program returned {ret} while 1 expected")
            self.assertEqual(output, err_string, f"Got output '{output}', expected '{err_string}'")

    def test_bad_input(self):
        data = [
            "",
            """this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long this key is too long"""
        ]
        err_string = "Invalid input"
        for key in data:
            output, ret = self._launch("main", key)
            self.assertEqual(ret, 1, f"Program returned {ret} while 1 expected")
            self.assertEqual(output, err_string, f"Got output: '{output}', expected: '{err_string}'")


if __name__ == "__main__":
    with open('report.xml', 'w') as report:
        unittest.main(testRunner=xmlrunner.XMLTestRunner(output=report), failfast=False, buffer=False, catchbreak=False)
