%include "colon.inc"

[section .rodata]

colon "third key", third
    db "third explanation", 0

colon "second key", second
    db "second explanation", 0

colon "first key", first
    db "first explanation", 0

__SECT__
