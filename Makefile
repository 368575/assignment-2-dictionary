CC     = nasm
CFLAGS = -f elf64

.PHONY: all clean test

all: main

main: main.o dict.o lib.o
	$(LD) -o $@ $^

main.o: main.asm dict.inc lib.inc words.inc
	${CC} ${CFLAGS} -o $@ $<

dict.o: dict.asm colon.inc lib.inc
	${CC} ${CFLAGS} -o $@ $<

lib.o: lib.asm
	${CC} ${CFLAGS} -o $@ $<

words.inc: colon.inc
	touch $@

clean:
	rm -f main main.o dict.o lib.o

test:
	python3 test.py
