%define list_head 0

%define PTR_OFFSET %[__BITS__ / 8]
%define KEY_OFFSET 0
%define DATA_OFFSET %[KEY_OFFSET + PTR_OFFSET]
%define NEXT_OFFSET %[DATA_OFFSET + PTR_OFFSET]

%macro colon 2
    %%node:
        dq %%key        ; key ptr
        dq %2           ; value ptr
        dq list_head    ; next node ptr
    %%key:
        db %1, 0
    %2:
    %define list_head %%node
%endmacro
