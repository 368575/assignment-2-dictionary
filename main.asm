%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define BUF_SIZE 255

global _start

section .rodata

err_input_msg:
    db "Invalid input", 0
err_keyword_msg:
    db "No such key string", 0

section .bss

buffer:
    resb BUF_SIZE + 1

section .text

_start:
    mov  rdi, buffer
    mov  rsi, BUF_SIZE
    call read_string
    test rax, rax
    jz   .error_input
    mov  rdi, rax
    mov  rsi, list_head
    call find_word
    test rax, rax
    jz   .error_search
    mov  rdi, [rax + DATA_OFFSET]
    call print_string
    xor  rdi, rdi
    jmp  .exit
.error_input:
    mov  rdi, err_input_msg
    jmp  .error_exit
.error_search:
    mov  rdi, err_keyword_msg
.error_exit:
    call print_error
    mov  rdi, 1
.exit:
    call exit
