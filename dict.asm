%include "colon.inc"
%include "lib.inc"

global find_word

section .text

; Поиск ключа в словаре.
; Принимает  два аргумента:
; - указатель на ключ - нуль-терминированную строку;
; - указатель на начало словаря.
; Если ключ найден, вернёт адрес элемента списка, иначе вернёт 0.
find_word:
    push r12
    push r13
    mov  r12, rdi
    mov  r13, rsi
.next:
    test r13, r13
    jz   .not_found
    mov  rdi, r12
    mov  rsi, [r13 + KEY_OFFSET]
    call string_equals
    test rax, rax
    jnz  .found
    mov  r13, [r13 + NEXT_OFFSET]
    jmp  .next
.not_found:
    xor  rax, rax
    jmp  .finish
.found:
    mov  rax, r13
.finish:
    pop  r13
    pop  r12
    ret
